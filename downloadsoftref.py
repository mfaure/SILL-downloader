#!/usr/bin/python
import os
import re
import sys
from urllib2 import urlopen, URLError, HTTPError, Request
import urllib2
import ssl

# Constantes
DOWNLOAD_OUTPUT_DIR = '/home/mfaure/Telechargements/ADULLACT_SILL_2016_raw_download'

strselect = '(\d{4}[ ][sS][pP]\d)|(\d+[.]\d+[.]\d+[.]\d+)|(\d+[.]\d+[.]\d+)|(\d+[.]\d+)'
# Exemple des 3 types de numerotation des versions geres :
# 0.91                    ]
# 45.2.0                  ] ---> (\d+[.]\d+[.]\d+[.]\d+)|(\d+[.]\d+[.]\d+)|(\d+[.]\d+)
# 21.0.0.242              ]
# 2016 SP1                ] pour draftsight (\d{4}[ ][sS][pP]\d)

if os.path.isfile("downloadsoftref.log"):  # initialisation du log avant chaque mise a jour
    os.remove("downloadsoftref.log")


class Logger(object):  # redirige les sorties sys.stdout et print vers un fichier de log
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)


sys.stdout = Logger("downloadsoftref.log")  # capture de la sortie ecran dans le fichier de log

# gestion du proxy

urllib2.install_opener(
    urllib2.build_opener(
        # urllib2.ProxyHandler({'http': 'proxyserv:80', 'https': 'proxyserv:80'})
        # a la place de proxyserv mettre le nom ou ip de votre proxy
    )
)
# gestion protocole securise

ssl._create_defaut_https_context = ssl._create_unverified_context  # pour eviter HTTP error : 503


# --------------------------------------------------------------------------------
def recupversion(strvers):
    try:
        url = urlopen(string_url)
        # , cafile="verisign.pem")
        latestvers = ""
        countline = 0
        for line in url:
            countline += 1
            if "numline" in strvers:  # pointer un numero de ligne avec le mot cle 'numline' dans stringversion
                if countline == int(strvers[strvers.find("numline") + 7:]):  # extraction et convertion numero ligne
                    hdlatestvers = re.search(strselect, line)  # recuperation version dans la ligne designee
                    if hdlatestvers > 0:
                        latestvers = hdlatestvers.group()
                        return latestvers

            if strvers in line:  # pour commencer la recherche apres la chaine de caracteres designee
                print line
                hdlatestvers = re.search(strselect, line[line.find(strvers):])
                if hdlatestvers > 0:
                    latestvers = hdlatestvers.group()
                    return latestvers
                else:  # la recherche est faite sur toute la ligne
                    # '2ieme methode recherche'
                    hdlatestvers = re.search(strselect, line)
                    if hdlatestvers > 0:
                        latestvers = hdlatestvers.group()
                        return latestvers
                    else:
                        break
        if latestvers == "":
            return "None"
        url.close()
    except HTTPError, e:
        if e.reason == 'Forbidden':
            req = urllib2.Request(string_url, headers={
                'User-Agent': "Magic Browser"})  # ou "Mozilla/5.0" pour eviter -> HTTP Error: Forbidden
            url = urllib2.urlopen(req)
        else:
            print "HTTP Error:", e.reason
    except URLError, e:
        print "URL Error:", e.reason


# -------------------------------------------------------------------------------
def dnlfile(url):
    # Open the url
    try:
        f = urlopen(url)
        # , cafile="Cybertrust_Global_Root.pem")
        print(
            "Downloading " + url + "\n"
            + "Fichier local : " + DOWNLOAD_OUTPUT_DIR + "/" + filename)

        if not os.path.exists(DOWNLOAD_OUTPUT_DIR):
            os.makedirs(DOWNLOAD_OUTPUT_DIR)

        with open(DOWNLOAD_OUTPUT_DIR + "/" + filename, "wb") as local_file:
            local_file.write(f.read())
        cfgfile = open("downloadsoftref.ini", 'w+')
        config.set(section, 'latestfile', filename)
        config.write(cfgfile)
        cfgfile.close()
    # handle errors
    except HTTPError, e:
        if e.reason == 'Forbidden':
            #            url.close()
            req = urllib2.Request(string_url, headers={
                'User-Agent': "Mozilla/5.0"})  # ou Magic Browser  pour eviter -> HTTP Error: Forbidden
            url = urllib2.urlopen(req)
        else:
            print "HTTP Error:", e.reason
            cfgfile = open("downloadsoftref.ini", 'w+')
            config.set(section, 'latestfile', e.reason)
            config.write(cfgfile)
            cfgfile.close()

    except URLError, e:
        print "URL Error:", e.reason, url
        cfgfile = open("downloadsoftref.ini", 'w+')
        config.set(section, 'latestfile', e.reason)
        config.write(cfgfile)
        cfgfile.close()


# -----------------------------------------------------------------------------
# 			programme principal
# -----------------------------------------------------------------------------

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0

config = ConfigParser()
config.read('downloadsoftref.ini')
listsections = config.sections()  # recuperation liste des sections

for section in listsections:
    string_url = config.get(section, 'URLversion')
    string_version = config.get(section, 'stringversion')
    latestverini = config.get(section, 'latestversion')
    oldversion = config.get(section, 'oldversion')
    latestfile = config.get(section, 'latestfile')
    print section
    #    print "string_version :",string_version
    latestversion = recupversion(string_version)  # recuperation de numero de la derniere version
    print "latestversion:", latestversion
    if latestversion == "None":  # si on recupere un numero de version
        print "chaine version vide!"
    #	continue
    else:
        filename = ''  # pour ne pas sortir le nom de fichier du logiciel si on ne trouve pas de version
        #	print "chaine version non vide"
        genfilename = config.get(section, 'filename')  # traitement du nom generique
        if genfilename.find('{version}') and latestversion != "None":  # numero de version entier a mettre
            if latestversion:
                filename = genfilename.replace('{version}',
                                               latestversion)  # fabrication du nom de fichier a telecharger
                if not os.path.isfile(DOWNLOAD_OUTPUT_DIR + "/" + genfilename.replace('{version}',
                                                                            latestversion)):
                    print('pas de fichier :', genfilename.replace('{version}', latestversion))
                    urldownloadgen = config.get(section, 'URLdownload')  # recuperation de url telechargement generique
                    if urldownloadgen != "":
                        posregex = urldownloadgen.find(
                            "re.search")  # gestion extrait version dans URL : expression reguliere python evaluee dans URL
                        version = latestversion  # parce que la variable version se trouve dans 're.search'
                        if posregex > 0:
                            debre = urldownloadgen[posregex:]
                            finchaine = urldownloadgen[debre.find('}') + int(posregex) + 1:]
                            expression = eval(debre[:debre.find('}')])  # position fin cmd regex
                            calculexpression = expression.group()  # resultat evaluation regex
                            debutchaine = urldownloadgen[:int(posregex) - 1]
                            urldownloadgen = debutchaine + calculexpression + finchaine

                        posreplace = urldownloadgen.find(
                            "version.replace")  # gestion delimiteur change dans version : methode 'replace' python evaluee dans URL
                        version = latestversion  # parce que la variable version se trouve dans 'version.replace'
                        while posreplace > 0:
                            if posreplace > 0:
                                debrep = urldownloadgen[posreplace:]
                                finchaine = urldownloadgen[debrep.find('}') + int(posreplace) + 1:]
                                expression = eval(debrep[:debrep.find('}')])
                                debutchaine = urldownloadgen[:int(posreplace) - 1]
                                urldownloadgen = debutchaine + expression + finchaine
                                posreplace = urldownloadgen.find("version.replace")
                        if urldownloadgen.find('{version}'):
                            chemintelechargement = urldownloadgen.replace('{version}', latestversion)
                            urldownloadgen = chemintelechargement
                        dnlfile(urldownloadgen)
                else:
                    print 'le fichier', filename, 'existe localement'
        cfgfile = open("downloadsoftref.ini", 'w+')  # mise a jour du fichier ini
        #	if oldversion == "":	# si pas ancienne version mettre la nouvelle version connue
        #	   config.set(section, 'oldversion',latestversion)
        if latestverini == "":  # si pas nouvelle version mettre la nouvelle version connue
            config.set(section, 'latestversion', latestversion)
        if latestverini != latestversion:  # si nouvelle version trouve mettre la precedente dans old
            config.set(section, 'oldversion', latestverini)
            config.set(section, 'latestversion', latestversion)  # la nouvelle dans latestversion
        if latestfile == "":
            config.set(section, 'latestfile', filename)
        config.write(cfgfile)
        cfgfile.close()

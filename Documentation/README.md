# SILL-Downloader Documentation

## Prérequis

* Python 2.7 (Python 3.x non testé)

## Proxy

Si un proxy doit être utilisé, le configurer directement dans le code (en attendant de rendre cela paramétrable) :

```
urllib2.ProxyHandler({'http': 'proxyserv:80', 'https': 'proxyserv:80'})
```
 
## Utilisation

1. Mettre à jour le fichier `downloadsoftref.ini`
1. Lancer `python downloadsoftref.py`
1. Les logiciels sont téléchargés dans le dossier `paquet-appli`
